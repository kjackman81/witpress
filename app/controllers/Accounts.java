package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Accounts extends Controller {

    public static void welcome() {
        render();
    }
    
    public static void signup()
    {
    	render();
    }
    
    public static void login()
    {
    	render();
    }
    
    public static void logout()
    {
    	session.clear();
    	Accounts.welcome();
    }
    
    public static void register(String email, String firstName, String lastName, String password)
    {
    	User user = new User(email, firstName, lastName, password);
    	user.save();
    	Logger.info(firstName + " " + lastName + " " + email + " " + password);
    	login();
    	
    }
    
	public static void authenticate(String email, String password)
    {
    	Logger.info("Attempting to authenticate with " + email + ":" + password);
    	
    	User user = User.findByEmail(email);
    	
    	if ((user != null) && (user.checkPassword(password) == true)) 
    	{

			session.put("logged_in_userid", user.id);

			Logger.info("You are good to GO!!!:  " + user.firstName + " " + user.lastName); 
			Blog.blog();
        }
    	else
    	{
    		Logger.info("Authentication failed");
			login();
    	}

    }
    
        public static User getCurrentUser() 
        {
		String userId = session.get("logged_in_userid");
		
		if (userId == null || userId.isEmpty()) {
			return null;
		}

		User logged_in_user = User.findById(Long.parseLong(userId));
		Logger.info("the logged in user is: " + logged_in_user.firstName);
		return logged_in_user;

	}
    
    
    
}