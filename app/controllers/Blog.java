package controllers;

import java.util.ArrayList;
import java.util.List;

import models.Comment;
import models.Post;
import models.User;
import play.Logger;
import play.mvc.Before;
import play.mvc.Controller;

public class Blog extends Controller {

	public static void blog() {
		User user = Accounts.getCurrentUser();

		List<Post> allPosts = Post.findAll();
		List<Post> myPosts = new ArrayList<Post>();

		for (Post p : allPosts) {
			if (p.from.id.equals(user.id)) {
				myPosts.add(p);
			}
		}

		render(user, myPosts);
	}

	/**
	 * This method executed before each action call in the controller. Checks
	 * that a user has logged in. If no user logged in the user is presented
	 * with the log in screen.
	 */
	@Before
	static void checkAuthentification() {
		if (session.contains("logged_in_userid") == false)
			Accounts.login();
	}

	public static void addPost(String title, String content) {
		Logger.info("title " + title + "  content: " + content);

		User user = Accounts.getCurrentUser();
		Post post = new Post(user, title, content);
		post.save();
		blog();

	}

	public static void deletePost(long id) {
		Logger.info("Posts id to be removed " + id);

		Post post = Post.findById(id);

		List<Comment> allComments = Comment.findAll();

		for (Comment c : allComments) {
			if (c.post.id.equals(id)) {
				c.delete();
			}
		}

		post.delete();

		blog();

	}

}
