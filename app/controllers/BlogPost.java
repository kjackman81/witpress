package controllers;

import java.util.ArrayList;
import java.util.List;

import models.Comment;
import models.Post;
import models.User;
import play.Logger;
import play.mvc.Before;
import play.mvc.Controller;

public class BlogPost extends Controller {

	public static void blogPost(long id) {
		Post post = Post.findById(id);

		List<Comment> allComments = Comment.findAll();

		List<Comment> comments = new ArrayList<Comment>();

		for (Comment c : allComments) {
			if ((c.post.id).equals(post.id)) {
				comments.add(c);
			}
		}

		Logger.info("posts title: " + post.title + " post content: "
				+ post.content);

		render(post, comments);
	}

	/**
	 * This method executed before each action call in the controller. Checks
	 * that a user has logged in. If no user logged in the user is presented
	 * with the log in screen.
	 */
	@Before
	static void checkAuthentification() {
		if (session.contains("logged_in_userid") == false)
			Accounts.login();
	}

	public static void addComment(String comContent, long id) {
		Logger.info("the comment is " + comContent + " the id is " + id);

		User user = Accounts.getCurrentUser();

		Post post = Post.findById(id);

		Comment comment = new Comment(comContent, post, user);
		comment.save();

		blogPost(id);
	}

	public static void deleteComment(long id) {

		Logger.info("Comment id to be removed " + id);

		Comment comment = Comment.findById(id);
		long postId = comment.post.id;

		comment.delete();

		blogPost(postId);

	}

}
