package controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import models.Comment;
import models.Post;
import models.User;
import play.mvc.Before;
import play.mvc.Controller;

public class PublicBlog extends Controller {

	public static void publicBlogDirectory() {
		Set<User> bloggers = new HashSet<User>();
		List<Post> allPosts = Post.findAll();

		for (Post p : allPosts) {
			User temp = User.findById(p.from.id);
			bloggers.add(temp);
		}

		render(bloggers);
	}

	public static void publicBlog(long id) {
		List<Post> allPosts = Post.findAll();

		User blogger = User.findById(id);

		List<Post> myPosts = new ArrayList<Post>();

		for (Post post : allPosts) {
			if (post.from.id.equals(id)) {
				myPosts.add(post);
			}
		}
		render(myPosts, blogger);
	}

	public static void privateBlogComments(long id) {
		List<Comment> allComments = new ArrayList<Comment>();
		List<Comment> postComments = new ArrayList<Comment>();
		Post post = null;

		post = Post.findById(id);
		allComments = Comment.findAll();

		postComments = new ArrayList<Comment>();

		for (Comment comment : allComments) {
			if (comment.post.id.equals(id)) {
				postComments.add(comment);
			}
		}

		render(post, postComments);
	}

	public static void publicBlogComments(long id) {
		List<Comment> allComments = new ArrayList<Comment>();
		List<Comment> postComments = new ArrayList<Comment>();
		Post post = null;

		if (session.contains("logged_in_userid") == false) {
			post = Post.findById(id);
			allComments = Comment.findAll();

			postComments = new ArrayList<Comment>();

			for (Comment comment : allComments) {
				if (comment.post.id.equals(id)) {
					postComments.add(comment);
				}
			}

			render(post, postComments);
		}

		else {
			privateBlogComments(id);
		}
	}

	public static void addPriComment(long id, String priComContent) {
		User user = Accounts.getCurrentUser();
		Post post = Post.findById(id);

		Comment comment = new Comment(priComContent, post, user);
		comment.save();
		privateBlogComments(id);
	}

}
