package models;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import play.db.jpa.Model;

@Entity
public class Comment extends Model {

	public String content;
	public Date date;

	@ManyToOne
	public Post post;

	@ManyToOne
	public User from;

	public Comment(String content, Post post, User from) {
		this.content = content;
		this.post = post;
		this.from = from;
		this.date = new Date();
	}

}
