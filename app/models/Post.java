package models;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import controllers.Accounts;
import play.Logger;
import play.db.jpa.Model;

@Entity
public class Post extends Model {

	public String content;
	public String title;
	public Date date;

	@ManyToOne
	public User from;

	public Post(User from, String title, String content) {
		this.content = content;
		this.title = title;
		this.from = from;
		this.date = new Date();
	}

}
