package models;

import java.util.List;

import javax.persistence.Entity;

import play.db.jpa.Model;

@Entity
public class User extends Model {

	public String email;
	public String firstName;
	public String lastName;
	public String password;

	public User(String email, String firstName, String lastName,
			String password) {

		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;

	}

	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}

	public static User findByEmail(String email) {
		return find("email", email).first();
	}

	@Override
	public String toString() {
		return this.firstName + " " + this.lastName;
	}

}
